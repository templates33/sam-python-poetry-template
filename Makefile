create-requirements:
	@poetry export -f requirements.txt -o layers/requirements.txt

build: create-requirements
	@sam build $(target)
